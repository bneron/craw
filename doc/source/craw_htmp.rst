.. _craw_htmp:

=========
craw_htmp
=========


`craw_htmp.py` is the entry point script to compute heatmap.

.. automodule:: craw.scripts.craw_htmp
    :members:
    :private-members:
    :special-members:

