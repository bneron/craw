.. _craw_coverage:

=============
craw_coverage
=============


`craw_coverage.py` is the entry point script to compute coverage.

.. automodule:: craw.scripts.craw_coverage
    :members:
    :private-members:
    :special-members:

