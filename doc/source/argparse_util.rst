.. _argparse_util:

=============
argparse_util
=============

Some utilities to improve the :mod:`argparse` module.

.. automodule:: craw.argparse_util
    :members:
    :private-members:
    :special-members:

