.. _craw:

====
craw
====

The *_get_version_message* is a private function that provide
a human readable version of `craw` package and `python`
which is common to all scripts.
Each script have a public function *get_version_message*
that call this function for the common part and add the version of all dependencies need for the script.



.. automodule:: craw
    :members:
    :private-members:
    :special-members:

