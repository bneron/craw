#!/usr/bin/env sh
set -e

CMD="$1"
shift
ARGS=${@}

case ${CMD} in
	coverage )
		exec /usr/local/bin/craw_coverage ${ARGS} ;;
	htmp )
		exec /usr/local/bin/craw_htmp ${ARGS} ;;
	* )
		echo "command \"${CMD}\" is not supported. available commands: \"coverage\"|\"htmp\""
    exit 127
    ;;
esac